import {Component, OnInit} from '@angular/core';
import {Exchange} from '../../../../models/exchanges';
import {ExchangeService} from '../../../../services/exchange.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-table',
  templateUrl: './home-table.component.html',
  styleUrls: ['./home-table.component.sass']
})
export class HomeTableComponent implements OnInit {

  list: Exchange[];
  sortField = 'NAME';
  defSortField = 'NAME';
  sortDirection = 1;

  sortFunctions = {
    name: (a, b) => {
      if (a.NAME.toLowerCase() > b.NAME.toLowerCase()) {
        return 1;
      }
      if (a.NAME.toLowerCase() < b.NAME.toLowerCase()) {
        return -1;
      }
      return 0;
    },

    volume: (a, b) => a.VOLUME - b.VOLUME,

    fiat: (a, b) => {
      if (a.FIAT < b.FIAT) {
        return 1;
      }
      if (a.FIAT > b.FIAT) {
        return -1;
      }
      return 0;
    },

    pairs: (a, b) => a.NUMBER_OF_PAIRS - b.NUMBER_OF_PAIRS
  };

  constructor(private exchangeService: ExchangeService) {
    this.list = this.exchangeService.getExchanges();
    this.list = this.changeFormat(this.list);
  }

  ngOnInit() {}

  sort() {
    this.list.sort((a, b) => {
      const res = this.sortFunctions[this.sortField](a, b);
      if (res === 0) {
        return this.sortFunctions[this.sortField](a, b);
      } else {
        return res * this.sortDirection;
      }
    });
  }

  changeSortField(field: string) {
    if (this.sortField === field) {
      this.sortDirection *= -1;
    } else {
      this.sortDirection = 1;
      this.sortField = field;
    }
    this.sort();
  }

  changeFormat(list): Exchange[] {
    list.map(item => {
      item.FIAT = item.FIAT !== 'none';
    });
    return list;
  }

}
