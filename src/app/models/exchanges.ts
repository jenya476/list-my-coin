export class Exchanges {
  exchange: Exchange[];
}

export class Exchange {
  ID: number;
  NAME: string;
  LINK: string;
  VOLUME: number;
  NUMBER_OF_PAIRS: number;
  FIAT: string;
  TETHER: boolean;
  JURISDICTION: string;
  ALLOWED_IN_US: boolean;
  DESCRIPTION: string;
  RATING: number;
  PAIRS: number;
  NOT_ALLOWED_IN: string;
  LIMITATIONS: string;
  SOCIALS: Social[];
}

class Social {
  NAME: string;
  LINK: string;
}
