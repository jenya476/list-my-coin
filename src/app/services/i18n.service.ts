import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class I18nService {
  readonly languages: string[] = ['en'];
  readonly defaultLang = 'en';
  constructor(private translate: TranslateService,
              private localStorage: LocalStorageService) {
    // this.translate.setDefaultLang(this.getLang());
    this.changeLocalization('en');
    // this.localStorage.deleteLocalStorageLang();
    this.setLocalization();
  }

  public changeLocalization(lang: string) {
    this.localStorage.setLocalStorageLang(lang);
    this.setLocalization();
  }

  public getLang(): string {
    const localLang = this.localStorage.getLocalStorageLang();
    const browserLang = this.translate.getBrowserLang();
    if (!!localLang && this.hasLocalLang(localLang)) {
      return localLang;
    } else if (this.hasLocalLang(browserLang)) {
      return  browserLang;
    } else {
      return  this.defaultLang;
    }
  }

  private setLocalization() {
    this.translate.use(this.getLang());
  }

  private hasLocalLang(lang: string): boolean {
    return !!this.languages.find(v => v === lang);
  }

}
