// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAVz1zyf0hpmTOHTazUGW_Sm5el0iBnWho',
    authDomain: 'list-my-coin.firebaseapp.com',
    databaseURL: 'https://list-my-coin.firebaseio.com',
    projectId: 'list-my-coin',
    storageBucket: 'list-my-coin.appspot.com',
    messagingSenderId: '768809374630'
  }
};
