import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Page404Component } from './pages/page-404/page-404.component';
import { HomeComponent } from './pages/home/home.component';
import {AppRoutingModule} from './app-routing.module';
import {MatButtonModule, MatDialogModule, MatIconModule, MatRippleModule, MatSnackBarModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {UtilsService} from './services/utils.service';
import {CustomLoader} from './services/i18n.loader';
import {I18nService} from './services/i18n.service';
import {LocalStorageService} from './services/local-storage.service';
import { TranslateLoader , TranslateModule } from '@ngx-translate/core';
import { ToolbarComponent } from './items/toolbar/toolbar.component';
import { FooterComponent } from './items/footer/footer.component';
import { HomeHeaderComponent } from './pages/home/areas/home-header/home-header.component';
import { RocketComponent } from './pages/home/items/rocket/rocket.component';
import { HomeTableComponent } from './pages/home/areas/home-table/home-table.component';
import { ExchangeComponent } from './pages/exchange/exchange.component';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../environments/environment';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {FirebaseService} from './services/firebase.service';
import {ExchangeService} from './services/exchange.service';
import {StarsModule} from './items/stars/stars.module';

@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    HomeComponent,
    ToolbarComponent,
    FooterComponent,
    HomeHeaderComponent,
    RocketComponent,
    HomeTableComponent,
    ExchangeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AppRoutingModule,
    MatRippleModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatButtonModule,
    StarsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {provide: TranslateLoader, useClass: CustomLoader}
    }),
  ],
  providers: [
    LocalStorageService,
    I18nService,
    CustomLoader,
    UtilsService,
    FirebaseService,
    ExchangeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
