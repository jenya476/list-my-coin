import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as gsap from 'gsap';

@Component({
  selector: 'app-rocket',
  templateUrl: './rocket.component.html',
  styleUrls: ['./rocket.component.sass']
})
export class RocketComponent implements OnInit {
  @ViewChild('1_1') circle1_1: ElementRef;
  @ViewChild('1_2') circle1_2: ElementRef;

  @ViewChild('1.5_1') circle1_5_1: ElementRef;
  @ViewChild('1.5_2') circle1_5_2: ElementRef;
  @ViewChild('1.5_3') circle1_5_3: ElementRef;
  @ViewChild('1.5_4') circle1_5_4: ElementRef;
  @ViewChild('1.5_5') circle1_5_5: ElementRef;
  @ViewChild('1.5_6') circle1_5_6: ElementRef;
  @ViewChild('1.5_7') circle1_5_7: ElementRef;
  @ViewChild('1.5_8') circle1_5_8: ElementRef;
  @ViewChild('1.5_9') circle1_5_9: ElementRef;
  @ViewChild('1.5_10') circle1_5_10: ElementRef;
  @ViewChild('1.5_11') circle1_5_11: ElementRef;

  @ViewChild('2_1') circle2_1: ElementRef;
  @ViewChild('2_2') circle2_2: ElementRef;
  @ViewChild('2_3') circle2_3: ElementRef;

  @ViewChild('2.5_1') circle2_5_1: ElementRef;
  @ViewChild('2.5_2') circle2_5_2: ElementRef;
  @ViewChild('2.5_3') circle2_5_3: ElementRef;

  @ViewChild('3_1') circle3_1: ElementRef;
  @ViewChild('3_2') circle3_2: ElementRef;

  @ViewChild('4_1') circle4_1: ElementRef;
  @ViewChild('4_2') circle4_2: ElementRef;
  @ViewChild('4_3') circle4_3: ElementRef;

  @ViewChild('5_1') circle5_1: ElementRef;
  @ViewChild('5_2') circle5_2: ElementRef;
  @ViewChild('5_3') circle5_3: ElementRef;
  @ViewChild('5_4') circle5_4: ElementRef;
  @ViewChild('5_5') circle5_5: ElementRef;
  @ViewChild('5_6') circle5_6: ElementRef;
  @ViewChild('5_7') circle5_7: ElementRef;
  @ViewChild('5_8') circle5_8: ElementRef;
  @ViewChild('5_9') circle5_9: ElementRef;
  @ViewChild('5_10') circle5_10: ElementRef;
  @ViewChild('5_11') circle5_11: ElementRef;
  @ViewChild('5_12') circle5_12: ElementRef;
  @ViewChild('5_13') circle5_13: ElementRef;
  @ViewChild('5_14') circle5_14: ElementRef;
  @ViewChild('5_15') circle5_15: ElementRef;
  @ViewChild('5_16') circle5_16: ElementRef;

  arr1 = [];
  arr1_5 = [];
  arr2 = [];
  arr2_5 = [];
  arr3 = [];
  arr4 = [];
  arr5 = [];

  // tl = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Sine.easeInOut});
  tl1 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});
  tl2 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});
  tl3 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});
  tl4 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});
  tl5 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});
  tl6 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});
  tl7 = new gsap.TimelineMax({ repeat: -1, yoyo: false, ease: gsap.Linear.easeNone});

  constructor() { }

  ngOnInit() {
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.arr1 = [this.circle1_1, this.circle1_2];
    this.arr1_5 = [this.circle1_5_1, this.circle1_5_2, this.circle1_5_3, this.circle1_5_4, this.circle1_5_5, this.circle1_5_6, this.circle1_5_7, this.circle1_5_8, this.circle1_5_9, this.circle1_5_10, this.circle1_5_11];
    this.arr2 = [this.circle2_1, this.circle2_2, this.circle2_3];
    this.arr2_5 = [this.circle2_5_1, this.circle2_5_2, this.circle2_5_3];
    this.arr3 = [this.circle3_1, this.circle3_2];
    this.arr4 = [this.circle4_1, this.circle4_2, this.circle4_3];
    this.arr5 = [this.circle5_1, this.circle5_2, this.circle5_3, this.circle5_4, this.circle5_5, this.circle5_6, this.circle5_7, this.circle5_8, this.circle5_9, this.circle5_10, this.circle5_11, this.circle5_12, this.circle5_13, this.circle5_14, this.circle5_15, this.circle5_16];

    for (let i = 0; i < this.arr1.length; ++i) {
      this.tl1.fromTo(
        this.arr1[i].nativeElement, 9,
        { x: w - this.itemPosition(this.arr1_5[i]), y: -300},
        { x: -this.itemPosition(this.arr1_5[i]), y: 300}, 0);
    }
    for (let i = 0; i < this.arr1_5.length; ++i) {
      this.tl2.fromTo(
        this.arr1_5[i].nativeElement, 5,
        {attr: {cx: w - this.itemPosition(this.arr1_5[i])}},
        {attr: {cx: -this.itemPosition(this.arr1_5[i])}}, 0);
    }
    for (let i = 0; i < this.arr2.length; ++i) {
      this.tl3.fromTo(
        this.arr2[i].nativeElement, 6,
        {x: w - this.itemPosition(this.arr2[i]), y: -300},
        {x: -this.itemPosition(this.arr2[i]), y: 300}, 0);
    }
    for (let i = 0; i < this.arr2_5.length; ++i) {
      this.tl4.fromTo(
        this.arr2_5[i].nativeElement, 5,
        {x: w - this.itemPosition(this.arr2_5[i]), y: 100},
        {x: -this.itemPosition(this.arr2_5[i]), y: 0}, 0);
    }
    for (let i = 0; i < this.arr3.length; ++i) {
      this.tl5.fromTo(
        this.arr3[i].nativeElement, 4,
        {x: w - this.itemPosition(this.arr3[i]), y: -200},
        {x: -this.itemPosition(this.arr3[i]), y: 200}, 0);
    }
    for (let i = 0; i < this.arr4.length; ++i) {
      this.tl6.fromTo(
        this.arr4[i].nativeElement, 2,
        {x: w - this.itemPosition(this.arr4[i]), y: -150},
        {x: -this.itemPosition(this.arr4[i]), y: 150}, 0);
    }
    for (let i = 0; i < this.arr5.length; ++i) {
      this.tl7.fromTo(
        this.arr5[i].nativeElement, 3,
        {x: w, y: -100},
        {x: -w, y: 100}, 0);
    }
  }

  itemPosition(item: ElementRef): number {
    const canvasW = 1200;
    const iw = window.innerWidth;
    const cx = item.nativeElement.attributes.cx.value;
    const posX = (cx * iw) / canvasW;
    return posX;
  }

}
