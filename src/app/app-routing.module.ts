import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { Page404Component } from './pages/page-404/page-404.component';
import {ExchangeComponent} from './pages/exchange/exchange.component';
import {AppComponent} from './app.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'exchange/:name', component: ExchangeComponent },
  { path: 'exchange', redirectTo: '/', pathMatch: 'full' },
  { path: '404', component: Page404Component },
  { path: '**', redirectTo: '404', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
