import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TranslateLoader } from '@ngx-translate/core';

export class CustomLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return Observable.of(transition[lang]);
  }
}
export interface LocalString {[lang: string]: string; }

const transition: any = {
  en: {
    'lang': 'en',

    hero_text_1: 'Quick and easy way',
    hero_text_2: 'to list your coin',
    hero_text_3: 'on major exchanges worldwide.',

    hero_btn_1: 'Request Listing',
    hero_btn_2: 'Chat with us',
    hero_btn_3: 'List my coin here',

    table_column_1: 'Name',
    table_column_2: '24h Volume',
    table_column_3: 'Fiat',
    table_column_4: 'Number of exchange pairs',

    label_jurisdiction: 'Jurisdiction',
    label_fiat: 'Fiat',
    label_allowed_in_us: 'Allowed in USA',
    label_not_allowed_in: 'Not allowed in',
    label_limitations: 'Limitations',
    label_volume: '24hr volume',
    label_rating: 'Rating',
    label_pairs: 'Pairs',
    label_tether: 'Tether',

    footer_text: 'You can list your own coin on the exchanges above. Just let us know your plans for the listing and our qualified personnel will manage all the details.',
    footer_btn: 'Request Listing',

    get_in_touch: 'Get in touch:',
    telegram: 'Telegram',
    twitter: 'Twitter',
    email: 'E-mail:',
    copy: 'All rights reserved, 2018',
    yes: 'Yes',
    no: 'No'

  }
};
