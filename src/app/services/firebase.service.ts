import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/shareReplay';

@Injectable()
export class FirebaseService {

  constructor(private db: AngularFireDatabase) { }

  getFirebaseData(): Observable<any> {
    // return this.db.object('Exchanges/').valueChanges().first();
    return this.db.object('Exchanges/').valueChanges().first().shareReplay(1);
  }

}
