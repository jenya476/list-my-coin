import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class UtilsService {
  readonly regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(public snackBar: MatSnackBar,
              private translate: TranslateService) {}

  snackBarActive(message, speed?) {
    this.snackBar.open(message, '', {
      duration: (speed) ? speed : 3500
    });
  }

  getValidateMessage(state: string): Observable<string> {
    return this.translate.get(state);
  }

  sendMailValidate(name: string, email: string) {
    if (!name || !name.trim()) {
      throw new Error('error_name');
    } else if (!email || !email.trim()) {
      throw new Error('error_email');
    } else if (!email.match(this.regEmail)) {
      throw new Error('error_email_not_valid');
    }
    return true;
  }
}
