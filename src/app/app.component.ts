import {Component, OnDestroy} from '@angular/core';
import {I18nService} from './services/i18n.service';
import {ExchangeService} from './services/exchange.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnDestroy {
  title = 'app';
  dataLoaded = false;
  private firebaseDataSubskription: Subscription;
  constructor(private i18nService: I18nService,
              private exchangeService: ExchangeService) {
  this.firebaseDataSubskription = this.exchangeService.getFirebaseData().subscribe( () => {
      this.dataLoaded = true;
    });
  }
  ngOnDestroy() {
    this.firebaseDataSubskription.unsubscribe();
  }


}
