import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ExchangeService} from '../../services/exchange.service';
import {Exchange} from '../../models/exchanges';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.sass']
})
export class ExchangeComponent implements OnInit {

  exchange: Exchange;

  constructor(private router: ActivatedRoute,
              private exchangeService: ExchangeService) {
    const name = this.router.snapshot.paramMap.get('name');
    this.exchange = this.exchangeService.getExchange(name);

  }



  ngOnInit() {
  }

}
