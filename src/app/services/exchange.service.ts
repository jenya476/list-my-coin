import { Injectable } from '@angular/core';
import {FirebaseService} from './firebase.service';
import {Exchange} from '../models/exchanges';
import 'rxjs/add/operator/do';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
  export class ExchangeService {

  exchanges: Exchange[] = [];
  exchange: Exchange;

  constructor(private firebaseService: FirebaseService) {}

  getFirebaseData(): Observable<Exchange[]> {
    return this.firebaseService.getFirebaseData().do((exchanges: Exchange[]) => {
      for (const ex of exchanges) {
        this.exchanges.push(ex);
      }
    }).first();
  }

  getExchanges(): Exchange[] {
    return this.exchanges;
  }

  getExchange(name: string): Exchange {
    this.exchange = this.exchanges.find(ex => ex.NAME === name);
    return this.exchange;
  }

}
