import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StarsComponent} from './stars.component';
import {MatIconModule} from '@angular/material';
import { RangePipe } from './range.pipe';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule
  ],
  declarations: [
    StarsComponent,
    RangePipe,
  ],
  exports: [StarsComponent]
})
export class StarsModule { }
