import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {
  private readonly USER_LANG_PATH = 'alterfolioLang';

  constructor() { }

  // lang

  public setLocalStorageLang(lang: string) {
    localStorage.setItem(this.USER_LANG_PATH, lang);
  }

  public getLocalStorageLang(): string {
    return localStorage.getItem(this.USER_LANG_PATH);
  }

  public deleteLocalStorageLang() {
    return localStorage.removeItem(this.USER_LANG_PATH);
  }

}
